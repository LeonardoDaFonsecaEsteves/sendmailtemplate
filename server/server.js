const express = require("express");
const app = express();
const port = 8080;
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");
const shell = require("shelljs");
const {CONF} = require("./env");
const {Decrypt} = require("./utils/crypting");
var fs = require("fs");
var handlebars = require("handlebars");

var readHTMLFile = function(path, callback) {
  fs.readFile(path, {encoding: "utf-8"}, function(err, html) {
    if (err) {
      throw err;
      callback(err);
    } else {
      callback(null, html);
    }
  });
};

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/** CONFIGURATION HEADER */
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  next();
});

/** ROUTE SEND LOGIN OR CONFIG PAGE USER */
app.get("/", (req, res) => {
  if (CONF) {
    res.send(true);
  } else {
    res.send(false);
  }
});

/** ROUTE CONNEXION */
app.post("/connect", (req, res) => {
  let confPass = "";
  let reqPass = "";
  Decrypt(CONF.PASS, returnCrypt => (confPass = returnCrypt));
  Decrypt(req.body.PASS, returnCrypt => (reqPass = returnCrypt));
  if (confPass !== "" && reqPass !== "") {
    if (CONF.USERNAME === req.body.USERNAME && confPass === reqPass) {
      res.send(true);
    } else {
      res.send(false);
    }
  }
});

let status = null;
/** SEND MAIL */
app.post("/sendMail", (req, res) => {
  let confP = "";
  Decrypt(CONF.PASS, pass => (confP = pass));
  if (confP !== "") {
    let transporter = nodemailer.createTransport({
      host: CONF.HOST,
      port: CONF.PORT,
      secure: CONF.SECURE,
      service: CONF.SERVICE,
      auth: {
        user: CONF.EMAIL,
        pass: confP,
      },
    });
    readHTMLFile(__dirname + "/template/template.html", function(err, html) {
      var template = handlebars.compile(html);
      var replacements = {
        subject: `<h1>${req.body.subject}</h1>`,
        body: req.body.html,
        user: `<p>${CONF.USERNAME}</p>`,
      };
      var htmlToSend = template(replacements);
      var mailOptions = {
        from: CONF.EMAIL,
        to: req.body.mailTo,
        subject: req.body.subject,
        html: htmlToSend,
      };
      transporter.sendMail(mailOptions, function(error, response) {
        if (error) {
          status = false;
        } else {
          status = true;
        }
        res.send(status);
      });
    });
  }
});
/** SET CONFIG MAIL INFO */
let secure = false;
app.post("/saveConfig", (req, res) => {
  shell.touch("env.js");
  if (req.body.PORT === 465) {
    secure = true;
  }
  shell
    .ShellString(
      `exports.CONF = {
         USERNAME : "${req.body.USERNAME}",
         EMAIL :"${req.body.EMAIL}",
         PASS  :"${req.body.PASS}",
         HOST  :"${req.body.HOST}",
         PORT  :${req.body.PORT},
         SECURE :${secure},
         SERVICE :"${req.body.SERVICE}"};`
    )
    .to("./env.js");
  res.send(true);
});

/** RESET CONFIG MAIL INFO */
app.delete("/deleteConfig", (req, res) => {
  shell.ShellString(`exports.CONF = null;`).to("./env.js");
  res.send(true);
});

/** LISTEN SERVER */
app.listen(port, () => {
  console.log(`listening at http://localhost:${port}`);
});
