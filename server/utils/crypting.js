const CryptoJS = require("crypto-js");

exports.Decrypt = (req, returnCrypt) => {
  const bytes = CryptoJS.AES.decrypt(req, "1234567890");
  const decrypted = bytes.toString(CryptoJS.enc.Utf8);
  returnCrypt(decrypted);
};
