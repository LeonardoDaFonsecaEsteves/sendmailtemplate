import React, {useState, useEffect} from "react";
import {getConnect} from "../../api/api";
import "../../styles/App.css";
import ConfigPage from "../pages/configPage";
import LoginPages from "../pages/LoginPages";

function App() {
  const [state, setState] = useState(false);

  useEffect(() => {
    getConnect().then(res => {
      setState(res);
    });
  }, []);

  return state ? <LoginPages /> : <ConfigPage />;
}

export default App;
