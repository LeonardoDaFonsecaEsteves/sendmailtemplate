import React, {useState} from "react";
import {postConfig} from "../../api/api";
import {Email, PassWord, Text} from "../templates/inputs";
import {SelectMailingConfig} from "../templates/selector";

const ConfigPage = () => {
  const [state, setstate] = useState();

  const handelChange = (key, value) => {
    setstate({...state, [key]: value});
  };
  const handleConfigSelect = value => {
    setstate({...state, ...value});
  };

  const handleSubmit = () => {
    if (state) {
      postConfig(state).then(res => {
        if (res) {
          window.location.reload();
        }
      });
    }
  };

  return (
    <div id="config" className="auth-wrapper">
      <div className="auth-inner">
        <h1>Veuillez configure votre connexion</h1>
        <div className="form-group">
          <Text
            value="Nom d'utilisateur"
            action={value => handelChange("USERNAME", value)}
            className="form-control"
          />
        </div>

        <div className="form-group">
          <Email
            action={value => handelChange("EMAIL", value)}
            className="form-control"
          />
        </div>

        <div className="form-group">
          <PassWord
            action={value => handelChange("PASS", value)}
            className="form-control"
          />
        </div>
        <div className="form-group">
          <SelectMailingConfig
            action={value => handleConfigSelect(value)}
            className="form-control"
          />
        </div>

        <button
          type="submit"
          className="btn btn-primary btn-block"
          onClick={() => handleSubmit()}
        >
          Enregistrer votre configuration
        </button>
      </div>
    </div>
  );
};

export default ConfigPage;
