import React, {useState, useEffect} from "react";
import {postMail} from "../../api/api";
import {Email, Text} from "../templates/inputs";
import Wysiwyg from "../templates/wysiwig";
import {AlertErrorMail, AlertSuccess} from "../templates/alert";
import {TIME} from "../config/tempTimeout";

const HomePage = ({logout}) => {
  const [state, setState] = useState();
  const [status, setStatus] = useState({err: false, succ: false});

  const handelChange = (key, value) => {
    setState({...state, [key]: value});
  };

  useEffect(() => {
    setTimeout(() => {
      setStatus({err: false, succ: false});
    }, TIME);
    clearTimeout(TIME);
  }, [status]);

  const sendMail = () => {
    if (state) {
      postMail(state.mailTo, state.html, state.subject).then(res => {
        if (res) {
          setStatus({err: false, succ: true});
        } else {
          setStatus({err: true, succ: false});
        }
      });
    } else {
      setStatus({err: true, succ: false});
    }
  };

  return (
    <div className="auth-wrapper">
      <div className="home-inner">
        {status.err && <AlertErrorMail />}
        {status.succ && <AlertSuccess />}
        <div className="home-header">
          <div className="email">
            <Email action={e => handelChange("mailTo", e)} />
          </div>
          <div className="sujet">
            <Text value="Sujet" action={e => handelChange("subject", e)} />
          </div>
        </div>
        <div className="home-wysiwyg">
          <Wysiwyg action={html => handelChange("html", html)} />
        </div>
        <div className="home-send">
          <button className="btn btn-danger" onClick={() => logout()}>
            Déconnexion
          </button>
          <button className="btn btn-primary " onClick={() => sendMail()}>
            Envoyé
          </button>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
