import React, {useState} from "react";
import {useEffect} from "react";
import {deleteConfig, postConnect} from "../../api/api";
import {TIME} from "../config/tempTimeout";
import {AlertErrorLogin} from "../templates/alert";
import {PassWord, Text} from "../templates/inputs";
import HomePage from "./homePage";

const LoginPages = () => {
  const [state, setState] = useState();
  const [isConnect, setIsConnect] = useState(false);
  const [loginFail, setLoginFail] = useState(false);

  useEffect(() => {
    if (loginFail)
      setTimeout(() => {
        setLoginFail(false);
      }, TIME);
    clearTimeout(TIME);
  }, [loginFail]);

  const handelChange = (key, value) => {
    setState({...state, [key]: value});
  };

  const handleSubmit = () => {
    if (state) {
      postConnect(state).then(res => {
        setIsConnect(res);
        if (!res) {
          setLoginFail(true);
        }
      });
    }
  };
  const handleReset = () => {
    deleteConfig().then(res => {
      if (res) {
        window.location.reload();
      }
    });
  };

  const logout = () => {
    setIsConnect(false);
  };

  return (
    <>
      {loginFail && <AlertErrorLogin />}
      {isConnect ? (
        <HomePage logout={() => logout()} />
      ) : (
        <div id="login" className="auth-wrapper">
          <div className="auth-inner">
            <h1>Connexion</h1>
            <div className="form-group">
              <Text
                value="Nom d'utilisateur"
                action={value => handelChange("USERNAME", value)}
                className="form-control"
              />
            </div>

            <div className="form-group">
              <PassWord
                action={value => handelChange("PASS", value)}
                className="form-control"
              />
            </div>

            <button className="btn btn-danger btn-block" onClick={() => handleReset()}>
              Rénitialiser
            </button>
            <button className="btn btn-primary btn-block" onClick={() => handleSubmit()}>
              Connexion
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default LoginPages;
