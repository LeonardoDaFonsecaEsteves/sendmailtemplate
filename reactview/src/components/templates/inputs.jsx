import React from "react";
import {Form} from "react-bootstrap";
import Crypt from "../../utils/crypting";

export const Email = ({action}) => {
  return (
    <Form.Control
      type="email"
      placeholder="Email"
      onChange={e => action(e.target.value)}
      required
    />
  );
};
export const PassWord = ({action}) => {
  const crypt = pass => {
    Crypt.CryptContent(pass, passCrypted => {
      action(passCrypted);
    });
  };

  return (
    <Form.Control
      type="password"
      placeholder="Mot de passe"
      onChange={e => crypt(e.target.value)}
      required
    />
  );
};

export const Text = ({action, value}) => {
  return (
    <Form.Control
      type="text"
      placeholder={value}
      onChange={e => action(e.target.value)}
      required
    />
  );
};
