import React from "react";
import {Button} from "react-bootstrap";

export const ButtonSuccess = ({value, action}) => {
  return (
    <Button variant="primary" onClick={() => action()}>
      {value}
    </Button>
  );
};
export const ButtonError = ({value, action}) => {
  return (
    <Button variant="danger" onClick={() => action()}>
      {value}
    </Button>
  );
};
