import React, {useState} from "react";
import {EditorState, convertToRaw} from "draft-js";
import {Editor} from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

const Wysiwyg = ({action}) => {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const onEditorStateChange = editorState => {
    setEditorState(editorState);
    action(draftToHtml(convertToRaw(editorState.getCurrentContent())));
  };

  return (
    <Editor
      editorState={editorState}
      wrapperClassName="wrapper"
      editorClassName="editor"
      onEditorStateChange={editorState => onEditorStateChange(editorState)}
      toolbar={toolbar}
    />
  );
};
export default Wysiwyg;

export const toolbar = {
  options: ["inline", "blockType", "fontSize", "list", "textAlign", "link", "history"],
  inline: {
    inDropdown: false,
    className: undefined,
    component: undefined,
    dropdownClassName: undefined,
    options: ["bold", "italic", "underline"],
    bold: {className: undefined},
    italic: {className: undefined},
    underline: {className: undefined},
    strikethrough: {className: undefined},
    monospace: {className: undefined},
  },
  blockType: {
    inDropdown: true,
    options: ["Normal", "H1", "H2", "H3", "H4", "H5", "H6", "Blockquote", "Code"],
    className: undefined,
    component: undefined,
    dropdownClassName: undefined,
  },
  fontSize: {
    options: [8, 9, 10, 11, 12, 14, 16, 18, 24, 30, 36, 48, 60, 72, 96],
    className: undefined,
    component: undefined,
    dropdownClassName: undefined,
  },
  list: {
    inDropdown: false,
    className: undefined,
    component: undefined,
    dropdownClassName: undefined,
    options: ["unordered", "ordered"],
    unordered: {className: undefined},
    ordered: {className: undefined},
  },
  textAlign: {
    inDropdown: false,
    className: undefined,
    component: undefined,
    dropdownClassName: undefined,
    options: ["left", "center", "right", "justify"],
    left: {className: undefined},
    center: {className: undefined},
    right: {className: undefined},
    justify: {className: undefined},
  },
  link: {
    inDropdown: false,
    className: undefined,
    component: undefined,
    popupClassName: undefined,
    dropdownClassName: undefined,
    showOpenOptionOnHover: true,
    defaultTargetOption: "_self",
    options: ["link", "unlink"],
    link: {className: undefined},
    unlink: {className: undefined},
    linkCallback: undefined,
  },
  history: {
    inDropdown: false,
    className: undefined,
    component: undefined,
    dropdownClassName: undefined,
    options: ["undo", "redo"],
    undo: {className: undefined},
    redo: {className: undefined},
  },
};
