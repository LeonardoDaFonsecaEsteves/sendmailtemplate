import React from "react";
import fileConfig from "../config/mailConfig.json";
import {Form} from "react-bootstrap";

export const SelectMailingConfig = ({action}) => {
  const testeValue = value => {
    if (value !== "Service de messagerie") {
      action(JSON.parse(value));
    }
  };
  return (
    <Form.Group controlId="SelectMailingConfig">
      <Form.Control as="select" onChange={e => testeValue(e.target.value)}>
        <option>Service de messagerie</option>
        {fileConfig.map((value, key) => {
          return (
            <option key={key} value={JSON.stringify(value)}>
              {value.SERVICE}
            </option>
          );
        })}
      </Form.Control>
    </Form.Group>
  );
};
