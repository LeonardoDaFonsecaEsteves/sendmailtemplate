import React from "react";
import {Alert} from "react-bootstrap";

export const AlertSuccess = () => {
  return (
    <Alert variant="success">
      <Alert.Heading>Email envoyé!</Alert.Heading>
    </Alert>
  );
};

export const AlertErrorMail = () => {
  return (
    <Alert variant="danger">
      <Alert.Heading>Houps! une erreur ses produit !</Alert.Heading>
    </Alert>
  );
};

export const AlertErrorLogin = () => {
  return (
    <Alert variant="danger">
      <Alert.Heading>
        Le nom d'utilisateur ou le mot de passe sont incorrect !
      </Alert.Heading>
    </Alert>
  );
};
