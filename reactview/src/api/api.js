import Axios from "axios";
const {Url} = require("./config/config");

export const getConnect = async () => {
  const res = await Axios({
    method: "get",
    url: Url + "/",
  });
  return res.data;
};

export const postConnect = async connect => {
  const res = await Axios({
    method: "post",
    url: Url + "/connect",
    data: connect,
  });
  return res.data;
};

export const postMail = async (mailTo, html, subject) => {
  const res = await Axios({
    method: "post",
    url: Url + "/sendMail",
    data: {
      mailTo: mailTo,
      html: html,
      subject: subject,
    },
  });
  return res.data;
};

export const postConfig = async config => {
  const res = await Axios({
    method: "post",
    url: Url + "/saveConfig",
    data: config,
  });
  return res.data;
};

export const deleteConfig = async () => {
  const res = await Axios({
    method: "delete",
    url: Url + "/deleteConfig",
  });
  return res.data;
};
